﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoVR : MonoBehaviour
{
    GvrHeadset vista;

    
    float velocidadCamina=1f;

    bool camina=false;

    bool caminaAngulo=false;

    [SerializeField]
    Animator aim1;

    [SerializeField]
    GameObject aim2;

    void Start()
    {
        vista= Camera.main.GetComponent<GvrHeadset>();

    }

    
    void Update()
    {
        //Movimiento con control
        if(Input.GetButtonDown("Fire1"))
        {
            camina=true;
            aim1.SetBool("camina",true);
            aim1.SetBool("parado",false);
            
        }
        if(Input.GetButtonUp("Fire1"))
        {
            camina=false;
            aim1.SetBool("camina",false);
            aim1.SetBool("parado",true);
        }
        
        

        if (camina)
        {
            Vector3 direccion=new Vector3(vista.transform.forward.x,0,vista.transform.forward.z).normalized*velocidadCamina*Time.deltaTime;
            Quaternion rotacion = Quaternion.Euler(new Vector3(0,-transform.rotation.eulerAngles.y,0));
            transform.Translate(rotacion* direccion);
            //
            // Vector3 direccion2=new Vector3(aim2.transform.forward.x,0,aim2.transform.forward.z).normalized*velocidadCamina*Time.deltaTime;
            // transform.Translate(rotacion* direccion2);
        }
    }
}
