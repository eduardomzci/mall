﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class gvrBoton : MonoBehaviour
{
    [SerializeField]
    Image imgCirculo;

    [SerializeField]
    UnityEvent gvrClick;

    [SerializeField]
    UnityEvent gvrExit;
    
    [SerializeField]
    float time=2.0f;

    bool gvrEstado;

    [SerializeField]
    float gvrTiempo;

    [SerializeField]
    GameObject texto=default;

    void Update()
    {
        if(gvrEstado)
        {
            gvrTiempo+=Time.deltaTime;
            imgCirculo.fillAmount=gvrTiempo/time;
        }
        if(gvrTiempo>time)
        {
            gvrClick.Invoke();
        }
        if(gvrTiempo==0)
        {
            gvrExit.Invoke();
        }
    }

    public void GvrOn()
    {
        gvrEstado =true;
    }

    public void GvrOff()
    {
        gvrEstado=false;
        gvrTiempo=0;
        imgCirculo.fillAmount=0;
    }

    public void TextoOn()
    {
        texto.SetActive(true);
    }

    public void TextoOff()
    {
        texto.SetActive(false);
    }

}
